export const data = [
  {
    id: 1,
    heading: "Thinking Designs",
    info: "Information is gathered by our design consultants and professionals to develop user stories that result in fluid visual experiences and increase consumer engagement.",
  },
  {
    id: 2,
    heading: "Enterprise Apps",
    info: "Cross-platform enterprise mobility solutions that are reliable and scalable and come with strong analysis panels, advanced administration panels, and other features",
  },
  {
    id: 3,
    heading: "Develop once & Deploy",
    info: "Create an app once, and it will function optimally on several operating systems, including iOS, Android, and Windows.",
  },
  {
    id: 4,
    heading: "Custom Android App Development",
    info: "Cross-platform desktop apps with lots of features that let your company interact with and reach a wider audience. Cross-platform mobile apps that are available on demand and equipped with geolocation, map integration, digital POD, mobile scanner, and other capabilities",
  },
  {
    id: 5,
    heading: "E-Commerce Apps",
    info: "Full-featured e-commerce applications that can operate on several platforms and include all of the fundamental e-commerce functions",
  },
  {
    id: 6,
    heading: "Multimedia Apps",
    info: "Build mobile entertainment and multimedia apps that can be used across a wide variety of mobile devices to reach the largest audience.",
  },
  {
    id: 7,
    heading: "Social Network Apps",
    info: "Robust social network applications that link a large user base across platforms and devices allow businesses to expand their reach.",
  },
  {
    id: 8,
    heading: "Field Force Apps",
    info: "Applications for mobile devices and tablets that track sales forces in the field, including employee attendance, location, and report tracking.",
  },
  {
    id: 9,
    heading: "Business Apps",
    info: "Cross-platform custom apps that are equipped with the proper capabilities to meet a business's requirements and problems",
  },
];
