import Style from "./hero.module.css";
import ArrowIcon from "../../assets/Arrow-2.svg";
import Link from "next/link";

export default function HeroSection() {
  return (
    <div className={Style.heroSection}>
      <p className={Style.heading}>
        Leveraging creative, and technical thinking to create Cross-platform
        applications
      </p>
      <p className={Style.para}>
        Become a leading brand by leveraging our development and consultancy
        services to develop impactful high-quality, secure, cost-effective
        cross-platform applications.
      </p>
      <div className={Style.btnDiv}>
        <Link href="#" className={Style.btn}>
          Talk to out Experts
        </Link>
        <img className={Style.arrowIcon} src={ArrowIcon.src} alt="statLayout" />
      </div>
    </div>
  );
}
