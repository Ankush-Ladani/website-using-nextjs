import Style from "./support.module.css";

import Symbol from "../../assets/image-15.png";
import Symbol2 from "../../assets/image-16.png";
import Name from "../../assets/image-17.png";
import Link from "next/link";
import ArrowIcon from "../../assets/Arrow.svg";

export default function Support() {
  return (
    <div className={Style.section}>
      <div className={Style.leftSection}>
        <p className={Style.heading}>App Support & Upgrade</p>
        <ul className={Style.list}>
          <li className={Style.listItem}>
            {" "}
            To guarantee reliable and excellent performance, we offer
            comprehensive cross-platform app support and maintenance.
          </li>
          <li className={Style.listItem}>
            Upgrade services to guarantee that your app complies with the most
            recent cross-platform development updates
          </li>
        </ul>
        <div className={Style.btnDiv}>
          <Link href="#" className={Style.btn}>
            <span>
              <span>Contact us</span>
              <img
                className={Style.arrowIcon}
                src={ArrowIcon.src}
                alt="statLayout"
              />
            </span>
          </Link>
        </div>
      </div>
      <div className={Style.rightSection}>
        <div className={Style.imgDiv1}>
          <img className={Style.Symbol} src={Symbol.src} alt="symbolImg" />
          <img className={Style.Symbol2} src={Symbol2.src} alt="symbolImg2" />
        </div>
        <div className={Style.imgDiv2}>
          <img src={Name.src} alt="nameImg" />
        </div>
      </div>
    </div>
  );
}
