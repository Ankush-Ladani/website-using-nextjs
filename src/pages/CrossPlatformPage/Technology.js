import Style from "./technology.module.css";

import mobileImg from "../../assets/Visuals.png";

export default function Technology() {
  return (
    <div className={Style.section}>
      <div className={Style.leftSection}>
        <img className={Style.img} src={mobileImg.src} alt="" />
      </div>
      <div className={Style.rightSection}>
        <p className={Style.heading}>
          We are capable of meeting the needs of our clients since our expertise
          spans a variety of cross-platform technologies.
        </p>
        <div className={Style.list}>
          <ul className={Style.leftList}>
            <li className={Style.listItem}>React Native</li>
            <li className={Style.listItem}>PhoneGap</li>
            <li className={Style.listItem}>Xamarin</li>
            <li className={Style.listItem}>Titanium</li>
          </ul>
          <ul className={Style.rightList}>
            <li className={Style.listItem}>Ionic</li>
            <li className={Style.listItem}>HTML5</li>
            <li className={Style.listItem}>Flutter</li>
            <li className={Style.listItem}>Sencha</li>
          </ul>
        </div>
      </div>
    </div>
  );
}
