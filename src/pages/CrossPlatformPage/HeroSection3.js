import hero3 from "./hero3.module.css";
import ArrowIcon from "../../assets/Arrow-2.svg";

export default function HeroSection3() {
  return (
    <div className={hero3.hero3Section}>
      <p className={hero3.heading}>Why Us ?</p>
      <p className={hero3.para}>
        We offer expertise in all aspects of cross-platform development,
        including technical viability, requirements gathering, coding, testing,
        and UX/UI. 
      </p>
      <div className={hero3.listDiv}>
        <ul className={hero3.leftList}>
          <li className={hero3.leftItem}>
            Innovative Approach with focus on your business goals and user
            experience
          </li>
          <li className={hero3.leftItem}>
            10+ years of experience in Android App development
          </li>
          <li className={hero3.leftItem}>
            50+ team of highly knowledgeable experts with extensive skills
          </li>
          <li className={hero3.leftItem}>
            Excellent Custom App Development Services.
          </li>
        </ul>
        <ul className={hero3.rightList}>
          <li className={hero3.rightItem}>Multiple Domains served</li>
          <li className={hero3.rightItem}>Robust project management</li>
          <li className={hero3.rightItem}>
            Best integration process and security management
          </li>
        </ul>
      </div>
      <div className={hero3.btnDiv}>
        <a className={hero3.btn} href="#">
          Reach Us
        </a>
        <img className={hero3.arrowIcon} src={ArrowIcon.src} alt="" />
      </div>
    </div>
  );
}
