import HeroSection from "./HeroSection";
import Statistics from "./Statistics";
import HeroSection3 from "./HeroSection3";
import Technology from "./Technology";
import Services from "./Services";
import Services2 from "./Services2";
import Support from "./Support";
import Offer from "./Offer";

export default function Index() {
  return (
    <>
      <HeroSection />
      <Statistics />
      <Technology />
      <Services />
      <Services2 />
      <Support />
      <Offer />
      {/* <HeroSection3 /> */}
    </>
  );
}
