import Style from "./offer.module.css";
import { data } from "@/utils/constants";

import divIcon from "../../assets/div-icon.svg";
import Rectangle from "../../assets/rectangle.svg";
import stars from "../../assets/auto_awesome.svg";

export default function Offer() {
  //   console.log(data);
  return (
    <div className={Style.section}>
      <p className={Style.heading}>What we Offer</p>
      <div className={Style.innerSection}>
        {data.map((item) => {
          return (
            <div className={Style.card}>
              <div className={Style.imgDiv}>
                <img
                  className={Style.divIcon}
                  src={divIcon.src}
                  alt="divIcon"
                />
                <img
                  className={Style.Rectangle}
                  src={Rectangle.src}
                  alt="Rectangle"
                />
                <img className={Style.stars} src={stars.src} alt="stars" />
              </div>
              <p className={Style.item_heading}> {item.heading} </p>
              <p className={Style.item_info}> {item.info} </p>
            </div>
          );
        })}
      </div>
    </div>
  );
}
