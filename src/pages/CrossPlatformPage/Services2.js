import Style from "./services2.module.css";
import mobileImg from "../../assets/Visuals.png";

export default function Services2() {
  return (
    <div className={Style.section}>
      <div className={Style.leftSection}>
        <img className={Style.img} src={mobileImg.src} alt="" />
      </div>
      <div className={Style.rightSection}>
        <p className={Style.heading}>App Migration</p>
        <ul className={Style.list}>
          <li className={Style.listItem}>
            We provide knowledge across a variety of frameworks to migrate your
            application from one platform to another, doing away with any
            complications.
          </li>
        </ul>
      </div>
    </div>
  );
}
