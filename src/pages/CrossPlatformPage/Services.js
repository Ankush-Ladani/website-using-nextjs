import Style from "./services.module.css";
import mobileImg from "../../assets/Visuals.png";

export default function Services() {
  return (
    <div className={Style.section}>
      <p className={Style.heading}>Our Services</p>
      <p className={Style.para}>
        Count on us for all your Android application needs, starting from
        strategic planning, development, and testing, to deployment, and
        maintenance.
      </p>
      <div className={Style.innerSection}>
        <div className={Style.leftSection}>
          <p className={Style.heading2}>App Consulting, UI UX & Development</p>
          <ul className={Style.list}>
            <li className={Style.listItem}>
              Our staff of skilled developers is aware of your business'
              requirements and can offer strategic advice on developing apps for
              cross-platform experiences.
            </li>
            <li className={Style.listItem}>
              In order to maintain uniformity across platforms and make sure
              that the programme has aesthetically pleasing and user-friendly
              functionality.
            </li>
            <li className={Style.listItem}>
              We create unique cross-platform applications that make use of
              touch, gesture, and a native appearance and feel while utilizing
              platform-specific capabilities.
            </li>
          </ul>
        </div>
        <div className={Style.rightSection}>
          <img className={Style.img} src={mobileImg.src} alt="" />
        </div>
      </div>
    </div>
  );
}
