import Img from "../../assets/zinieeer-1.png";
import Style from "./statistics.module.css";

export default function Statistics() {
  return (
    <div className={Style.hero2Section}>
      <div className={Style.leftSection}>
        <p className={Style.heading}>
          Bring your ideas into reality by guiding you through each stage of the
          journey
        </p>
        <p className={Style.para}>
          We at Startxlabs, develop cross-platform mobile apps that provide a
          consistent user experience so that your clients will have the same
          dynamic experience whether they use an Android phone, an iPhone, an
          iPad, or even a laptop. We have professional app developers who have
          years of expertise working on platforms like Configure.IT, Ionic,
          React Native, and Xamarin. 
        </p>
        <p className={Style.stats}>some stats here</p>
      </div>
      <div className={Style.rightSection}>
        <img className={Style.img} src={Img.src} alt="" />
      </div>
    </div>
  );
}
